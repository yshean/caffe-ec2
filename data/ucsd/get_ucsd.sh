#!/usr/bin/env sh
# This scripts unzips the downloaded archive of ucsd dataset.

DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd $DIR

echo "Unzipping..."

gunzip ucsd-train-spatial.tar.gz
gunzip ucsd-test-spatial.tar.gz

# Creation is split out because leveldb sometimes causes segfault
# and needs to be re-created.

echo "Done."

# After running this code, there will be train_spatial and test_spatial folders in this directory.