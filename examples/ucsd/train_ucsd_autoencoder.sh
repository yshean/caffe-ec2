#!/usr/bin/env sh

./build/tools/caffe train \
  --solver=examples/ucsd/ucsd_autoencoder_solver.prototxt
