#!/usr/bin/env sh
# Create the ucsd lmdb inputs
# N.B. set the path to the ucsd train + test data dirs

EXAMPLE=examples/ucsd
DATA=data/ucsd
TOOLS=build/tools

TRAIN_DATA_ROOT=data/ucsd/train_spatial/60_40/slice02/
TEST_DATA_ROOT=data/ucsd/test_spatial/60_40/slice02/

# Set RESIZE=true to resize the images to 256x256. Leave as false if images have
# already been resized using another tool.
RESIZE=false
if $RESIZE; then
  RESIZE_HEIGHT=30
  RESIZE_WIDTH=20
else
  RESIZE_HEIGHT=30
  RESIZE_WIDTH=20
fi

if [ ! -d "$TRAIN_DATA_ROOT" ]; then
  echo "Error: TRAIN_DATA_ROOT is not a path to a directory: $TRAIN_DATA_ROOT"
  echo "Set the TRAIN_DATA_ROOT variable in create_ucsd.sh to the path" \
       "where the UCSD training data is stored."
  exit 1
fi

if [ ! -d "$TEST_DATA_ROOT" ]; then
  echo "Error: TEST_DATA_ROOT is not a path to a directory: $TEST_DATA_ROOT"
  echo "Set the TEST_DATA_ROOT variable in create_ucsd.sh to the path" \
       "where the UCSD train data is stored."
  exit 1
fi

echo "Creating train lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --shuffle \
    $TRAIN_DATA_ROOT \
    $DATA/train.txt \
    $EXAMPLE/ucsd_train_lmdb

echo "Creating test lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
    --shuffle \
    $TEST_DATA_ROOT \
    $DATA/test.txt \
    $EXAMPLE/ucsd_test_lmdb

echo "Done."
