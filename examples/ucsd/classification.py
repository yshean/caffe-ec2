import numpy as np
import matplotlib.pyplot as plt

caffe_root = '/opt/caffe/'

import sys

sys.path.insert(0, caffe_root + 'python')

import caffe

MODEL_FILE = '/home/ucsd_train_autoencoder.prototxt'
PRETRAINED = '/home/ucsd_autoencoder_iter_80000.caffemodel'
IMAGE_FILE = '/home/test/0153.JPG'

# caffe.set_mode_cpu()
caffe.set_mode_gpu()

## Error: https://github.com/BVLC/caffe/issues/1936
# net = caffe.Classifier(MODEL_FILE, PRETRAINED,
#                       mean=np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1),
#                       channel_swap=(2,1,0),
#                       raw_scale=255,
#                       image_dims=(256, 256))

net = caffe.Classifier(MODEL_FILE, PRETRAINED,
                       mean=np.load('/home/wood_mean.npy').mean(1).mean(1),
                       channel_swap=(2,1,0),
                       raw_scale=255,
                       image_dims=(256, 256))



## Fixed of the error above
# net = caffe.Classifier(MODEL_FILE, PRETRAINED)
# net.set_raw_scale('data',255)
# net.set_channel_swap('data',(2,1,0))
# net.set_mean('data',np.load(caffe_root+'python/caffe/imagenet/ilsvrc_2012_mean.npy'))

input_image = caffe.io.load_image(IMAGE_FILE)

plt.imshow(input_image)
plt.savefig('/home/input_image.png')
# plt.clf() # Clear figure
plt.close() # Close a figure window

prediction = net.predict([input_image])  # predict takes any number of images, and formats them for the Caffe net automatically
print 'prediction shape:', prediction[0].shape
plt.plot(prediction[0])
plt.savefig('/home/prediction.png')
print 'predicted class:', prediction[0].argmax()
plt.close()

