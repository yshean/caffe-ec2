# Training autoencoder on UCSD with Caffe

We will assume that you have Caffe successfully compiled. If not, please refer to the [Installation page](/installation.html). In this tutorial, we will assume that your Caffe installation is located at `CAFFE_ROOT`.

## Prepare Datasets

You will first need to download and convert the data format from Github. To do this, simply run the following commands:

    cd $CAFFE_ROOT
    ./examples/ucsd/create_ucsd.sh

#### TODO: rename the directory names of dataset folders
#### TODO: prepare testing dataset

If it complains that `gunzip` is not installed, you need to install it or unzip it in the directory of the archive file. After running the script there should be two datasets, `ucsd_train_lmdb`, and `ucsd_test_lmdb`.

## Autoencoder: the UCSD Anomaly Detection Model

Before we actually run the training program, let's explain what will happen. We will use autoencoder (modified from MNIST autoencoder example), which is known to work well on finding anomalous data. We will use a slightly different version from the original autoencoder implementation, replacing the sigmoid activations with Rectified Linear Unit (ReLU) activations for the neurons.

We have defined the layers in `$CAFFE_ROOT/examples/ucsd/ucsd_autoencoder.prototxt`.

## Define the autoencoder network

This section explains the `ucsd_autoencoder.prototxt` model definition that specifies the autoencoder model for UCSD anomaly detection task. We assume that you are familiar with [Google Protobuf](https://developers.google.com/protocol-buffers/docs/overview), and assume that you have read the protobuf definitions used by Caffe, which can be found at `$CAFFE_ROOT/src/caffe/proto/caffe.proto`.

Specifically, we will write a `caffe::NetParameter` (or in python, `caffe.proto.caffe_pb2.NetParameter`) protobuf. We will start by giving the network a name:

    name: "UCSDAutoencoder"

### Writing the Data Layer

Currently, we will read the UCSD data from the lmdb we created earlier in the demo. This is defined by a data layer:

    layers {
    	top: "data"
    	name: "data"
    	type: DATA
    	data_param {
    		source: "examples/ucsd/ucsd_train_lmdb"
    		backend: LMDB
    		batch_size: 100
    	}
    	transform_param {
    		scale: 0.0039215684
    	}
    	include: { phase: TRAIN }
    }

Specifically, this layer has name `ucsd`, type `data`, and it reads the data from the given lmdb source. We will use a batch size of 100, and scale the incoming pixels so that they are in the range \[0,1\). Why 0.0039215684? It is 1 divided by 256. And finally, this layer produces one blob, that is, the `data` blob.
Additionally, we also define the two extra data layers for testing phase.

	layers {
  		top: "data"
  		name: "data"
  		type: DATA
  		data_param {
    		source: "examples/ucsd/ucsd_train_lmdb"
    		backend: LMDB
    		batch_size: 100
  		}
  		transform_param {
    		scale: 0.0039215684
  		}
  		include: {
    		phase: TEST
    		stage: 'test-on-train'
  		}
	}
	
	layers {
  		top: "data"
		name: "data"
  		type: DATA
  		data_param {
    		source: "examples/ucsd/ucsd_test_lmdb"
    		backend: LMDB
    		batch_size: 100
  		}
  		transform_param {
    		scale: 0.0039215684
  		}
  		include: {
    		phase: TEST
    		stage: 'test-on-test'
  		}
	}

### Writing the Encoding Layer

Before we encode the data, let's flatten the input data into one flat vector:

	layers {
		bottom: "data"
		top: "flatdata"
		name: "flatdata"
		type: FLATTEN
	}

Let's define the first encoding layer:

	layers {
  		bottom: "data"
	  	top: "encode1"
  		name: "encode1"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 1000
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}
	
	layers {
  		bottom: "encode1"
  		top: "encode1neuron"
  		name: "encode1neuron"
  		type: SIGMOID
	}

This layer takes the `data` blob (it is provided by the data layer), and produces the `encode1` layer. It produces outputs of 1000 channels.

The fillers allow us to randomly initialize the value of the weights and bias. For the bias filler, we will simply initialize it as constant, with the default filling value 0.

`blobs_lr` are the learning rate adjustments for the layer's learnable parameters. In this case, we will set the weight and bias learning rate to be the same as the learning rate given by the solver during runtime.

# encode2 layer:

	layers {
  		bottom: "encode1neuron"
  		top: "encode2"
  		name: "encode2"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 500
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}
	
	layers {
  		bottom: "encode2"
  		top: "encode2neuron"
  		name: "encode2neuron"
  		type: SIGMOID
	}
	
# encode3 layer:

	layers {
  		bottom: "encode2neuron"
  		top: "encode3"
  		name: "encode3"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 250
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}
	
	layers {
  		bottom: "encode3"
  		top: "encode3neuron"
  		name: "encode3neuron"
  		type: SIGMOID
	}

# encode4 layer:

	layers {
  		bottom: "encode3neuron"
  		top: "encode4"
  		name: "encode4"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 30
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}

### Writing the Decoding Layer

Phew. Decoding layers are actually pretty similar to the encoding layer:

# decode4 layer:

	layers {
  		bottom: "encode4"
  		top: "decode4"
  		name: "decode4"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 250
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}

	layers {
  		bottom: "decode4"
  		top: "decode4neuron"
  		name: "decode4neuron"
  		type: SIGMOID
	}
	
# decode3 layer:
	
	layers {
  		bottom: "decode4neuron"
  		top: "decode3"
  		name: "decode3"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 500
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}
	
	layers {
  		bottom: "decode3"
  		top: "decode3neuron"
  		name: "decode3neuron"
  		type: SIGMOID
	}
	
# decode2 layer:

	layers {
  		bottom: "decode3neuron"
  		top: "decode2"
  		name: "decode2"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 1000
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}
	
	layers {
  		bottom: "decode2"
  		top: "decode2neuron"
  		name: "decode2neuron"
  		type: SIGMOID
	}

# decode1 layer:

	layers {
  		bottom: "decode2neuron"
  		top: "decode1"
  		name: "decode1"
  		type: INNER_PRODUCT
  		blobs_lr: 1
  		blobs_lr: 1
  		weight_decay: 1
  		weight_decay: 0
  		inner_product_param {
    		num_output: 784
    		weight_filler {
      			type: "gaussian"
      			std: 1
      			sparse: 15
    		}
    		bias_filler {
      			type: "constant"
      			value: 0
    		}
  		}
	}

Decode1 is the final layer of our autoencoder network. Finally we have a loss layer:

# loss layer for decode1:
	
	layers {
  		bottom: "decode1"
  		bottom: "flatdata"
  		top: "cross_entropy_loss"
  		name: "loss"
  		type: SIGMOID_CROSS_ENTROPY_LOSS
  		loss_weight: 1
	}

	layers {
  		bottom: "decode1"
  		top: "decode1neuron"
  		name: "decode1neuron"
  		type: SIGMOID
	}
	
	layers {
  		bottom: "decode1neuron"
  		bottom: "flatdata"
  		top: "l2_error"
  		name: "loss"
  		type: EUCLIDEAN_LOSS
  		loss_weight: 0
	}

Note that the order of the decoding layers is the descending order of the encoding layers.
Similarly, you can write up more encoding and decoding layers. Check `$CAFFE_ROOT/examples/ucsd/ucsd_autoencoder.prototxt` for details.
Each `innerproduct` layer is a fully connected layer (for some legacy reason, Caffe calls it an `innerproduct` layer) with number of outputs defined at `num_output`. All other lines look familiar, right?

The `cross_entropy_loss` and `euclidean_loss` layer does not produce any outputs - all it does is to compute the loss function value, report it when backpropagation starts, and initiates the gradient with respect to `decode1`. This is where all magic starts.


### Additional Notes: Writing Layer Rules

Layer definitions can include rules for whether and when they are included in the network definition, like the one below:

    layers {
      // ...layer definition...
      include: { phase: TRAIN }
    }

This is a rule, which controls layer inclusion in the network, based on current network's state.
You can refer to `$CAFFE_ROOT/src/caffe/proto/caffe.proto` for more information about layer rules and model schema.

In the above example, this layer will be included only in `TRAIN` phase.
If we change `TRAIN` with `TEST`, then this layer will be used only in test phase.
By default, that is without layer rules, a layer is always included in the network.
Thus, `ucsd_autoencoder.prototxt` has three `DATA` layers defined (with different `batch_size`), one for the training phase and two for the testing phase.

## Define the Autoencoder Solver

#### TODO: Modify number of iterations!!

Check out the comments explaining each line in the prototxt `$CAFFE_ROOT/examples/ucsd/ucsd_autoencoder_solver.prototxt`:

	# The autoencoder protocol buffer definition
	net: "examples/ucsd/ucsd_autoencoder.prototxt"
	# test_iter specifies how many forward passes the test should carry out.
	# In the case of UCSD, we have test batch size 500 on train dataset and 100 on test dataset,
	# and __ test iterations, covering the full ____ training and ____ testing images.
	test_state: { stage: 'test-on-train' }
	test_iter: 500
	test_state: { stage: 'test-on-test' }
	test_iter: 100
	# Carry out testing every 500 training iterations.
	test_interval: 500
	test_compute_loss: true
	# The base learning rate, momentum and the weight decay of the network.
	base_lr: 0.01
	weight_decay: 0.0005
	momentum: 0.95
	# The learning rate policy
	lr_policy: "step"
	gamma: 0.1
	stepsize: 10000
	# Display every 100 iterations
	display: 100
	# The maximum number of iterations
	max_iter: 65000
	# Snapshot intermediate results
	snapshot: 10000
	snapshot_prefix: "examples/ucsd/ucsd_autoencoder_nesterov_train"
	# solver mode: CPU or GPU
	solver_mode: GPU
	# Using Nesterov method
	solver_type: NESTEROV

## Training and Testing the Model

Training the model is simple after you have written the network definition protobuf and solver protobuf files. Simply run `train_autoencoder.sh`, or the following command directly:

    cd $CAFFE_ROOT
    ./examples/ucsd/train_ucsd_autoencoder.sh

`train_ucsd_autoencoder.sh` is a simple script, but here is a quick explanation: the main tool for training is `caffe` with action `train` and the solver protobuf text file as its argument.

#### TODO: modify the following part accordingly, take note of the real output after running.

When you run the code, you will see a lot of messages flying by like this:

    I1203 net.cpp:66] Creating Layer conv1
    I1203 net.cpp:76] conv1 <- data
    I1203 net.cpp:101] conv1 -> conv1
    I1203 net.cpp:116] Top shape: 20 24 24
    I1203 net.cpp:127] conv1 needs backward computation.

These messages tell you the details about each layer, its connections and its output shape, which may be helpful in debugging. After the initialization, the training will start:

    I1203 net.cpp:142] Network initialization done.
    I1203 solver.cpp:36] Solver scaffolding done.
    I1203 solver.cpp:44] Solving LeNet

Based on the solver setting, we will print the training loss function every 100 iterations, and test the network every 1000 iterations. You will see messages like this:

    I1203 solver.cpp:204] Iteration 100, lr = 0.00992565
    I1203 solver.cpp:66] Iteration 100, loss = 0.26044
    ...
    I1203 solver.cpp:84] Testing net
    I1203 solver.cpp:111] Test score #0: 0.9785
    I1203 solver.cpp:111] Test score #1: 0.0606671

For each training iteration, `lr` is the learning rate of that iteration, and `loss` is the training function. For the output of the testing phase, score 0 is the accuracy, and score 1 is the testing loss function.

And after a few minutes, you are done!

    I1203 solver.cpp:84] Testing net
    I1203 solver.cpp:111] Test score #0: 0.9897
    I1203 solver.cpp:111] Test score #1: 0.0324599
    I1203 solver.cpp:126] Snapshotting to lenet_iter_10000
    I1203 solver.cpp:133] Snapshotting solver state to lenet_iter_10000.solverstate
    I1203 solver.cpp:78] Optimization Done.

The final model, stored as a binary protobuf file, is stored at

    lenet_iter_10000

which you can deploy as a trained model in your application, if you are training on a real-world application dataset.

### Um... How about GPU training?

You just did! All the training was carried out on the GPU. In fact, if you would like to do training on CPU, you can simply change one line in `autoencoder_solver.prototxt`:

    # solver mode: CPU or GPU
    solver_mode: CPU

and you will be using CPU for training. Isn't that easy?

UCSD is a small dataset, so training with GPU does not really introduce too much benefit due to communication overheads. On larger datasets with more complex models, such as ImageNet, the computation speed difference will be more significant.

### How to reduce the learning rate a fixed steps?
Look at `ucsd_multistep_solver.prototxt`
#### TODO: Modify `lenet_multistep_solver.prototxt`